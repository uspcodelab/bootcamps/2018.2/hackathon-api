'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('HackathonEditions', {
      id: {
        primaryKey: true,
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      theme: {
        type: Sequelize.STRING
      },
      location: {
        type: Sequelize.STRING
      },
      requirement: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      rules: {
        type: Sequelize.TEXT
      },
      personal_data: {
        type: Sequelize.JSON
      },
      sponsor: {
        type: Sequelize.ARRAY(Sequelize.STRING)
      },
      registration_start: {
        type: Sequelize.DATE
      },
      registration_end: {
        type: Sequelize.DATE
      },
      date_start: {
        type: Sequelize.DATE
      },
      date_end: {
        type: Sequelize.DATE
      },
      judging_criteria: {
        type: Sequelize.ARRAY(Sequelize.TEXT)
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('HackathonEditions');
  }
};
