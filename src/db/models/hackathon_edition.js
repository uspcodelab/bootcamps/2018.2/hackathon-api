'use strict';
module.exports = (sequelize, DataTypes) => {
  const HackathonEdition = sequelize.define('HackathonEdition', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    theme: DataTypes.STRING,
    location: DataTypes.STRING,
    requirement: DataTypes.STRING,
    description: DataTypes.TEXT,
    rules: DataTypes.TEXT,
    personal_data: DataTypes.JSON,
    sponsor: DataTypes.ARRAY(DataTypes.STRING),
    registration_start: DataTypes.DATE,
    registration_end: DataTypes.DATE,
    date_start: DataTypes.DATE,
    date_end: DataTypes.DATE,
    judging_criteria: DataTypes.ARRAY(DataTypes.TEXT)
  },

  {
    indexes: [
      { unique: true, fields: ['id'] },
      { unique: true, fields: ['name', 'hackathonId'] },
      { fields: ['date_start'] }
    ]
  });

  HackathonEdition.associate = function(models) {
    HackathonEdition.belongsTo(models.Hackathon, { foreignKey: 'hackathonId' });
    HackathonEdition.hasMany(models.Prize, { foreignKey: 'edition' });
  };

  return HackathonEdition;
};
