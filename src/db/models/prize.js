'use strict';
module.exports = (sequelize, DataTypes) => {
  const Prize = sequelize.define('Prize', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    description: DataTypes.TEXT,
    name: DataTypes.STRING,
    rank: DataTypes.INTEGER
  },
  {
    indexes: [
      { unique: true, fields: ['id'] }
    ]
  });

  Prize.associate = function(models) {
    Prize.belongsTo(models.HackathonEdition, { foreignKey: 'edition' });
  };

  return Prize;
};
