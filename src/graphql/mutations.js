const mutations = `
  type Mutation {
    newHackathon(
      name: String!,
      owner_id: ID!,
    ): Hackathon!

    newHackathonEdition(
      name: String!,
      theme: String,
      location: String,
      requirement: String,
      description: String,
      rules: String,
      personal_data: String,
      sponsor: [String!],
      registration_start: DateInput,
      registration_end: DateInput,
      date_start: DateInput,
      date_end: DateInput,
      judging_criteria: [String!],
      hackathon_id: ID!,
    ): HackathonEdition!

    newPrize(
      description: String,
      name: String!,
      rank: Int!,
      hackathon_edition_id: ID!
    ): Prize!


    updateHackathon(
      id: ID!,
      name: String,
      owner_email: String,
    ): Hackathon!

    updateHackathonEdition(
      id: ID!,
      name: String,
      theme: String,
      location: String,
      requirement: String,
      description: String,
      rules: String,
      personal_data: String,
      sponsor: [String!],
      registration_start: DateInput,
      registration_end: DateInput,
      date_start: DateInput,
      date_end: DateInput,
      judging_criteria: [String!],
    ): HackathonEdition!

    updatePrize(
      id: ID!,
      description: String,
      name: String,
      rank: Int,
    ): Prize!

    deleteHackathon(id: ID!): Hackathon!

    deleteHackathonEdition(id: ID!): HackathonEdition!

    deletePrize(id: ID!): Prize!
  }
`;

export default mutations;
