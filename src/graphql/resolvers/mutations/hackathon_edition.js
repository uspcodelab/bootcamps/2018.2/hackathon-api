import { ApolloError } from 'apollo-server-koa';

import { formatDate } from '../../utils'
import UUID from 'uuid/v1';

export default {
  async newHackathonEdition(parent, args, context, info) {
    if (!args.hackathon_id)
      throw new ApolloError('Must have hackathon ID', 400);

    const { Hackathon, HackathonEdition } = context.db;

    const hackathon = await Hackathon.findById(args.hackathon_id)

    if (!hackathon) throw new ApolloError('Hackathon was not found', 404);

    let payload = {...args};
    payload.active = true;
    payload.id = UUID();

    //Validate name 
    if (payload.name.length < 3) 
      throw new ApolloError('Name must have at least 3 characters', 400);

    //TODO: Validate input

    const dates =
        ['registration_start', 'registration_end', 'date_start', 'date_end']
    dates.forEach(date => {
      if (payload[date]) {
        const {day, month, year} = payload[date];
        payload[date] = `${year}/${month}/${day}`;
      }
    })

    // Do actual creation
    const [hackathonEdition, isNewRecord] =
        await HackathonEdition.findOrCreate({
          where: {name: args.name, hackathonId: args.hackathon_id},
          defaults: payload
        });
    if (!isNewRecord)
      throw new ApolloError('Hackathon Edition already exists', 400);

    let data = hackathonEdition.dataValues;
    formatDate(data);

    return data;
  },

  async updateHackathonEdition(parent, args, context, info) {
    const { Hackathon, HackathonEdition } = context.db;
    const hackathon = await HackathonEdition.findById(args.id);

    if (!hackathonEdition)
      throw new ApolloError('Hackathon Edition was not found', 404);

    let payload = {...args};

    // TODO: Manipulate and validate inputs

    const dates =
        ['registration_start', 'registration_end', 'date_start', 'date_end']
    dates.forEach(date => {
      if (payload[date]) {
        const {day, month, year} = payload[date];
        payload[date] = `${year}/${month}/${day}`;
      }
    })

    if (args.name) payload.name = args.name;

    // TODO: Must verify if the update will break unique contraint

    // Do actual update
    await hackathon.updateAttributes(payload);

    return HackathonEdition.dataValues;
  },

  async deleteHackathonEdition(parent, args, context, info) {
    const { Hackathon, HackathonEdition } = context.db;
    const hackathonEdition = await HackathonEdition.findById(args.id);

    if (!hackathonEdition)
      throw new ApolloError('Hackathon Edition was not found', 404);

    // Disable 'active' flag
    await hackathonEdition.updateAttributes({active: false});

    return hackathonEdition.dataValues;
  }
}
