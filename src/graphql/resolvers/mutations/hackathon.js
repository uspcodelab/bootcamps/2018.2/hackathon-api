import { ApolloError } from "apollo-server-koa";
import UUID from 'uuid/v1';


export default {
  async newHackathon(parent, args, context, info) {
    if (!args.name || !args.owner_id)
      throw new ApolloError ("Fields cannot be blank", 400);

    const { Hackathon, User } = context.db;

    const user = await User.findById(args.owner_id)

    if (!user)
      throw new ApolloError("No such user found", 400);

    await Hackathon.sync();
    let hackathon = await Hackathon.findOne({
      where: {
        name: args.name,
        ownerId: user.dataValues.id
      },
    });

    if (hackathon)
      throw new ApolloError("Hackathon already exists", 400);

    hackathon = {
      id: UUID(),
      name: args.name
    }

    if (process.env.NATS_PRESENT != 'false') {
      console.log(process.env);
      const hackathonStr = JSON.stringify(hackathon);
      const buff = Buffer.allocUnsafe(hackathonStr.length);
      buff.fill(hackathonStr);

      try {
        await context.nats.publish("new_hackathon", buff);
      } catch(err) {
        throw new ApolloError(err.stack);
      }

      return hackathon;
    } else {
      hackathon = await Hackathon.create(hackathon);
      return hackathon.dataValues;
    }
  },

  async updateHackathon(parent, args, context, info) {
    const { Hackathon } = context.db;
    const hackathon = await Hackathon.findById(args.id);
    let payload = {};

    if (args.name)
      payload.name = args.name;

    //TODO: implement update for owner change
    //if (args.owner_email)

    await hackathon.updateAttributes(payload);

    return hackathon.dataValues;
  },

  async deleteHackathon(parent, args, context, info) {
    const { Hackathon } = context.db;
    const hackathon = await Hackathon.findById(args.id);

    await hackathon.updateAttributes({
        active: false
    });

    return hackathon.dataValues;
  }
}
