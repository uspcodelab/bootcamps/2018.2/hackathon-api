import {makeExecutableSchema} from 'graphql-tools';

import Mutations from './mutations';
import Queries from './queries';
import resolvers from './resolvers/';

const typeDefs = `
  type Hackathon {
    id: ID!
    name: String!
    editions: [HackathonEdition!]
    owner: User!
  }

  type HackathonEdition {
    id: ID!
    name: String!
    theme: String
    location: String
    requirement: String
    description: String
    rules: String
    personal_data: String
    sponsor: [String!]
    registration_start: Date
    registration_end: Date
    date_start: Date
    date_end: Date
    judging_criteria: [String!]
    hackathon: Hackathon!
    prizes: [Prize!]
  }

  type Prize {
    id: ID!
    name: String!
    description: String
    rank: Int!
  }

  type User {
    id: ID!
    hackathons: [Hackathon!]
  }

  input DateInput {
    day: Int!
    month: Int!
    year: Int!
  }

  type Date {
    day: Int!
    month: Int!
    year: Int!
  }

  ${Queries}
  ${Mutations}
`

export {
  typeDefs, resolvers
}
