import db from '../src/db/models';
import hackathonQuery from '../src/graphql/resolvers/queries/next_hackathons';
import hackathonMutation from '../src/graphql/resolvers/mutations/hackathon';
import editionMutation from '../src/graphql/resolvers/mutations/hackathon_edition';
import { ApolloError } from 'apollo-server-koa'

const context = {
  db
};

beforeEach(async () => {
  await db.Hackathon.create({
    id: '8ebc8363-26e0-4a72-90f2-3a3940c63e0d',
    name: 'Hackathon USP'
  });

  await db.User.create({
    id: '8e332191-2933-4b03-93b2-a3a30e0244f3'
  })
})

afterEach(async () => {
  await db.Hackathon.destroy({
    where: {}
  })

  await db.User.destroy({
    where: {}
  });

})

afterAll(async () => db.sequelize.close() );

test('Create a hackathon', async () => {
  const args = {
    name: "Hackatona",
    owner_id: '8e332191-2933-4b03-93b2-a3a30e0244f3'
  };

  const hack = await hackathonMutation.newHackathon(null, args, context, null);
  expect(hack.id).toBeDefined();
});

test('Query a hackathon', async () => {
  const value = await hackathonQuery.hackathon(null, {
    id: '8ebc8363-26e0-4a72-90f2-3a3940c63e0d'
  }, context, null);

  expect(value.name).toBe('Hackathon USP')
});

test('Destroy a hackathon', async () => {
  const args = {
    id: '8ebc8363-26e0-4a72-90f2-3a3940c63e0d'
  };

  const hack =
    await hackathonMutation.deleteHackathon(null, args, context, null);

  const miss = await hackathonQuery.hackathon(null, {
    id: '8ebc8363-26e0-4a72-90f2-3a3940c63e0d'
  }, context, null);

  expect(miss.active).toBeFalsy();
});

test("Can't find nonexistent hackathon", async () => {
  try {
    const value = await hackathonQuery.hackathon(null, {
      id: '26e0-4a72-90f2-3a3940c63e0d'
    }, context, null);
  } catch (e) {
    expect(e.extensions.code).toBe(400);
  }
});

//===================TESTS FOR HACKATHON EDITION==============
test("Create edition empty name", async() => {
  const args = {
    name: "",
    hackathon_id: '8ebc8363-26e0-4a72-90f2-3a3940c63e0d'
  };

  try {
    const hack_edition = await editionMutation.newHackathonEdition(null, args, context, null);
  } catch (e) {
    expect(e.extensions.code).toBe(400);
  }
});