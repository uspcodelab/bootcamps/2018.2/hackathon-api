# Introduction
This API is responsible for the management of hackathons. This includes querying, creating, updating, and deleting hackathons from the system. As well as managing objects such as hackathon editions, which are themselves separate entities.

---

# Tools and dependencies used
## Apollo Server
Apollo server is a GraphQL API generator with a series of helpers that speed up the development, like an SDL to define types, queries and mutations. It also takes care of the HTTP requests.

## Koa Adapter
Koa is an extremely small Node.JS framework used to create APIs. Koa only takes care of the routing, which is everything we need. Apollo Server will take care of parsing the requests, passing them to the proper resolver. 

## Sequelize
Sequelize is an ORM (Object Relational Mapper), which means it generates SQL for us. This enable us to write JavaScript for querying instead of pure SQL.

## Jest
Jest is a javascript test framework made by Facebook, it's very good and easy to learn. It is used to build and run unit tests.

## GraphQL
GraphQL is a querying language and protocol used to make all requests to the back-end. It allows requests to receive only the required information, thus saving data and work.

## NATS
NATS is a message broker. It is responsible handling message relaying between the back-end microservices.

## Docker
Docker is a platform for easy containerization, application distribution and deployment. Info at: https://www.docker.com/why-docker

---

# How to get started?
Build the docker image by running:
```
docker-compose build --no-cache --force-rm
```

And start it with:
```
docker-compose up
```

---

# What are all these files?
* `src/server.js` holds the configuration for Koa, Apollo Server, and NATS. This file is the program entrypoint.

* `src/db/` holds all sequelize files.
  * `src/db/config.js` holds all configurations for the Sequelize instance.
  * `src/db/models/` holds all the files with our defined models.
  * `src/db/migrations/` holds all the database migrations.
  * `src/db/seeders/` holds all the files with our seeds definitions.

* `src/graphql/` holds all graphql files
  * `src/schema.js` holds all our types definitions, combine these definitions with queries and mutations. And exports this combinations and the resolvers.
  * `src/graphql/queries.js` holds all our query definitions.
  * `src/graphql/mutations.js` holds all our mutation definitions.
  * `src/graphql/resolvers/` holds all our resolver files. Resolvers are the functions that are called when a query or mutation is called.
* `test` holds all test files.